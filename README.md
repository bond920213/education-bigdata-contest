# Education BigData Contest

NKNU 112-1 教育大數據分析專題製作期末競賽

## 資料來源 [均一 kaggle dataset](https://www.kaggle.com/datasets/junyiacademy/learning-activity-public-dataset-by-junyi-academy/data?select=Log_Problem.csv)

## SQL [Backup](https://drive.google.com/file/d/1DfZYJwpYTuAGpdD5gWHMo3F5-G-81SgP/view?usp=sharing)
使用PostgreSQL為後端資料庫，備份檔如上