'''
Created on 2023年12月31日

@author: bond9
'''
import psycopg2
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# 建立到 PostgreSQL 的連接
conn = psycopg2.connect(
    database='Education_BigData',
    user='postgres',
    password='bondfct920213',
    host='localhost',
    port='5432'
)

# 設定中文字型（使用 matplotlib 內建的字體）
plt.rcParams['font.sans-serif'] = ['SimSun']  # 替換成你所使用的中文字型
plt.rcParams['axes.unicode_minus'] = False  # 解決保存图像是负号'-'显示为方块的问题

# 查詢使用者在均一學院的教師人數為0的學生的答題率
query_answer_rate_self_study = """
    SELECT
        AVG(CASE WHEN is_correct THEN 1 ELSE 0 END) AS answer_rate
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt = 0;
"""

# 查詢使用者在均一學院的教師人數不為0的學生的答題率
query_answer_rate_teacher_guidance = """
    SELECT
        AVG(CASE WHEN is_correct THEN 1 ELSE 0 END) AS answer_rate
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt > 0;
"""

# 查詢使用者在均一學院的教師人數為0的學生的平均用時
query_avg_time_taken_self_study = """
    SELECT
        AVG(total_sec_taken) AS avg_time_taken
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt = 0;
"""

# 查詢使用者在均一學院的教師人數不為0的學生的平均用時
query_avg_time_taken_teacher_guidance = """
    SELECT
        AVG(total_sec_taken) AS avg_time_taken
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt > 0;
"""

# 查詢使用者在均一學院的教師人數為0的學生的提示使用率
query_hint_usage_rate_self_study = """
    SELECT
        AVG(CASE WHEN is_hint_used THEN 1 ELSE 0 END) AS hint_usage_rate
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt = 0;
"""

# 查詢使用者在均一學院的教師人數不為0的學生的提示使用率
query_hint_usage_rate_teacher_guidance = """
    SELECT
        AVG(CASE WHEN is_hint_used THEN 1 ELSE 0 END) AS hint_usage_rate
    FROM
        "Log_Problem" lp
        JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
    WHERE
        has_teacher_cnt > 0;
"""

# 查詢自學和受老師指導學生在嘗試次數的關係
query_attempt_count = """
    -- 計算每個學生的嘗試次數
    WITH AttemptCounts AS (
        SELECT
            uuid,
            COUNT(*) AS attempt_count
        FROM
            "Log_Problem"
        GROUP BY
            uuid
    )

    -- 區分自學和受教師指導的學生
    , LearningType AS (
        SELECT
            ud.uuid,
            CASE WHEN ud.has_teacher_cnt > 0 THEN 'TeacherGuidance' ELSE 'SelfStudy' END AS learning_type
        FROM
            "Info_UserData" ud
    )

    -- 最終結果
    SELECT
        lt.learning_type,
        AVG(ac.attempt_count) AS avg_attempt_count
    FROM
        LearningType lt
        LEFT JOIN AttemptCounts ac ON lt.uuid = ac.uuid
    GROUP BY
        lt.learning_type;
"""

# 查詢比較兩種類別學生在學習後是否能夠有效吸收知識
Performance_Ratio_By_Category = """
-- 計算每個學生的答對題數
    WITH CorrectAnswers AS (
        SELECT
            uuid,
            COUNT(*) AS correct_count
        FROM
            "Log_Problem"
        WHERE
            is_correct = true
        GROUP BY
            uuid
    )

    -- 計算每個學生的有效觀看時間
    , EffectiveWatchTime AS (
        SELECT
            lp.uuid,
            SUM(lp.total_sec_taken) AS total_watch_time
        FROM
            "Log_Problem" lp
            JOIN "Info_UserData" ud ON lp.uuid = ud.uuid
        WHERE
            lp.total_sec_taken > 0 -- 假設有效觀看時間必須大於0
        GROUP BY
            lp.uuid
    )

    -- 統計答對題數和有效觀看時間的比例
    , PerformanceRatio AS (
        SELECT
            ca.uuid,
            ca.correct_count,
            ewt.total_watch_time,
            COALESCE(CAST(ca.correct_count AS float) / NULLIF(ewt.total_watch_time, 0), 0) AS performance_ratio
        FROM
            CorrectAnswers ca
            LEFT JOIN EffectiveWatchTime ewt ON ca.uuid = ewt.uuid
    )

    -- 區分自學和受教師指導的學生
    , LearningType AS (
        SELECT
            ud.uuid,
            CASE WHEN ud.has_teacher_cnt > 0 THEN 'TeacherGuidance' ELSE 'SelfStudy' END AS learning_type
        FROM
            "Info_UserData" ud
    )

    -- 最終結果
    SELECT
        lt.learning_type,
        AVG(pr.correct_count) AS avg_correct_count,
        AVG(pr.total_watch_time) AS avg_total_watch_time,
        AVG(pr.performance_ratio) AS avg_performance_ratio
    FROM
        LearningType lt
        LEFT JOIN PerformanceRatio pr ON lt.uuid = pr.uuid
    GROUP BY
        lt.learning_type;
"""

# 將查詢結果轉換為 Pandas DataFrame
df_answer_rate_self_study = pd.read_sql(query_answer_rate_self_study, conn)
df_answer_rate_teacher_guidance = pd.read_sql(query_answer_rate_teacher_guidance, conn)

df_avg_time_taken_self_study = pd.read_sql(query_avg_time_taken_self_study, conn)
df_avg_time_taken_teacher_guidance = pd.read_sql(query_avg_time_taken_teacher_guidance, conn)

df_hint_usage_rate_self_study = pd.read_sql(query_hint_usage_rate_self_study, conn)
df_hint_usage_rate_teacher_guidance = pd.read_sql(query_hint_usage_rate_teacher_guidance, conn)

df_attempt_count = pd.read_sql(query_attempt_count, conn)

# 效果比例(performance_ratio)的計算方式是將學生答對的題數（correct_count）除以其觀看的總時間（total_watch_time)
print("querying performance ratio...")
df_performance_ratio = pd.read_sql(Performance_Ratio_By_Category, conn)
print("self_study")
print(df_answer_rate_self_study, end="\n\n")
print("teacher_guidance")
print(df_answer_rate_teacher_guidance, end="\n\n")
print("self_study")
print(df_avg_time_taken_self_study, end="\n\n")
print("teacher_guidance")
print(df_avg_time_taken_teacher_guidance, end="\n\n")
print("self_study")
print(df_hint_usage_rate_self_study, end="\n\n")
print("teacher_guidance")
print(df_hint_usage_rate_teacher_guidance, end="\n\n")
print(df_performance_ratio, end="\n\n")

# 繪製三個子圖
fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(10, 18))

# 答題率
axes[0].bar(['自學', '受教師指導'],
            [df_answer_rate_self_study['answer_rate'].values[0], df_answer_rate_teacher_guidance['answer_rate'].values[0]],
            color=['blue', 'orange'])
axes[0].set_title('答題率')
axes[0].set_ylabel('數值')

# 平均用時
axes[1].bar(['自學', '受教師指導'],
            [df_avg_time_taken_self_study['avg_time_taken'].values[0], df_avg_time_taken_teacher_guidance['avg_time_taken'].values[0]],
            color=['green', 'red'])
axes[1].set_title('平均用時')
axes[1].set_ylabel('數值')

# 提示使用率
axes[2].bar(['自學', '受教師指導'],
            [df_hint_usage_rate_self_study['hint_usage_rate'].values[0], df_hint_usage_rate_teacher_guidance['hint_usage_rate'].values[0]],
            color=['purple', 'brown'])
axes[2].set_title('提示使用率')
axes[2].set_ylabel('數值')



# 添加layout
plt.tight_layout()

# 設定 subplot 的位置
plt.subplots_adjust(left=0.3, bottom=0.05, right=0.7, top=0.93, wspace=0.2, hspace=0.11)

# 顯示圖形
plt.show()

# 繪製嘗試次數的長條圖
plt.bar(df_attempt_count['learning_type'], df_attempt_count['avg_attempt_count'], color=['blue', 'orange'])
plt.title('嘗試次數')
plt.ylabel('平均嘗試次數')
plt.show()

# 繪製效果比例
fig, ax = plt.subplots(figsize=(10, 6))

ax.bar(df_performance_ratio['learning_type'], df_performance_ratio['avg_performance_ratio'], color=['cyan', 'magenta'])
ax.set_title('效果比例')
ax.set_ylabel('平均效果比例')

# 顯示圖形
plt.show()

# 關閉數據庫連接
conn.close()